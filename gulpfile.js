var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var babel       = require('gulp-babel');

gulp.task('babel', function() {
  return gulp.src('src/scripts-babel/**/*.js')
  .pipe(babel())
  .pipe(gulp.dest('src/scripts'));
});


gulp.task('browserSync', function() {
  browserSync.init({
    notify: false,
    // Customize the Browsersync console logging prefix
    logPrefix: 'APP',
    // Allow scroll syncing across breakpoints
    // scrollElementMapping: ['main', '.mdl-layout'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    proxy: "localhost:8080",
    host: "192.168.0.16"
  });
  // gulp.watch(['src/**/*.html', 'src/'], browserSync.reload);
});

gulp.task('watch', ['browserSync'], function() {
  gulp.watch('src/**/*.html', browserSync.reload);
  gulp.watch('src/scripts-babel/**/*.js', ['babel'], browserSync.reload)
});
